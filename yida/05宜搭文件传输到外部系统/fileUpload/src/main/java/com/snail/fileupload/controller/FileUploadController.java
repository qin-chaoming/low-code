package com.snail.fileupload.controller;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import com.snail.fileupload.*;
import com.snail.fileupload.service.UrlChangeService;
import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.StatusLine;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.ContentType;
import org.apache.http.entity.StringEntity;
import org.apache.http.entity.mime.HttpMultipartMode;
import org.apache.http.entity.mime.MultipartEntityBuilder;
import org.apache.http.entity.mime.content.FileBody;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClientBuilder;

import org.dom4j.util.StringUtils;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.multipart.MultipartHttpServletRequest;

import javax.servlet.http.HttpServletRequest;
import javax.xml.bind.DatatypeConverter;
import java.io.*;
import java.nio.charset.StandardCharsets;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import java.util.UUID;

@Controller
@RequestMapping("/file")
public class FileUploadController {

    public String infoUrl = "";

    public String projectUrl = "";

    public String fileUrl = "";

    @RequestMapping(value = "/upload", method = RequestMethod.POST)
    @ResponseBody
    public void fileUpload(HttpServletRequest request) {

        try {
            request.setCharacterEncoding("UTF-8");
            StringBuilder buffer = new StringBuilder();
            BufferedReader reader = null;
            try {
                reader = new BufferedReader(new InputStreamReader(request.getInputStream(), "UTF-8"));
                String line = null;
                while ((line = reader.readLine()) != null) {
                    buffer.append(line);
                }
            } catch (Exception e) {
                e.printStackTrace();
            } finally {
                if (null != reader) {
                    try {
                        reader.close();
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                }
            }
            JSONObject jsonObject = JSONObject.parseObject(buffer.toString());
            String downloadURL = jsonObject.getJSONObject("data").getJSONArray("url").getJSONObject(0).getString("downloadURL");
//            String downloadURL="https://buisj7.aliwork.com/ossFileHandle?appType=APP_RTK54WVT34NVI4Y68C5K&fileName=APP_RTK54WVT34NVI4Y68C5K_MTQ1ODQwMjQzOTEwNDUyOTZfQjc4NjZUODFPRVVBN1JITUVCOEpGOFhYME5MVjI5RllBNFJITDc$.doc&instId=&type=download&originalFileName=2ae1d23095102f30b29ada82ee114fca.doc";
            String userid = jsonObject.getString("userid");
            String systemToken = jsonObject.getString("systemToken");

            String pjNum = jsonObject.getJSONObject("data").getString("projectNumber");//项目编号
            String pjName = jsonObject.getJSONObject("data").getString("projectName");//项目名称

            System.out.println(pjNum);
            System.out.println(pjName);

//            String userid="5198651";
//            String systemToken="JT966BB1RUZ0P2TY9HJ2H9J6J51P2YBF4524LX5";
            String lastDownloadURL = "https://www.aliwork.com" + downloadURL;
            //调用钉钉生成免密路径 首先获取token
            System.out.println(AccessTokenCommon.ACCESS_TOKEN);
            //获取路径
            Map<String, String> urlParms = new HashMap<String, String>() {{
                put("accessToken", AccessTokenCommon.ACCESS_TOKEN);
                put("key", systemToken);
                put("userId", userid);
                put("originalUrl", lastDownloadURL);
                put("appType", "APP_WFPA8B7Q47FP20P13CRD");

            }};

            String downloadUrl = UrlChangeService.changeUrl(urlParms);
            String a = downloadUrl;
            System.out.println(downloadUrl);
            if(!"".equals(downloadUrl)){
                //获取用户信息
                HttpUtils.get(infoUrl);
                UserInfoVo userInfoVo = JSONObject.parseObject(HttpUtils.get(infoUrl),UserInfoVo.class);
                //创建项目
                if(null!=userInfoVo){
                    Map<String, String> prarms = new HashMap<>();
                    Map<String, String> docAttrs = new HashMap<>();
                    Map<String, String> headers = new HashMap<>();
//                    String pjNum= UUID.randomUUID().toString().replace("-", "");
//                    String pjName=pjNum+new Date().getTime();
                    prarms.put("projectNumber",pjNum);
                    prarms.put("projectName", pjName);
                    prarms.put("projectManager", "ULTAdmin");
                    String jsonPrarms = JSON.toJSONString(prarms);
                    CloseableHttpClient httpClient = HttpClientBuilder.create().build();   //获取浏览器信息
                    HttpPost httpPost = new HttpPost(projectUrl);
                    String encoding = DatatypeConverter.printBase64Binary((userInfoVo.getUserName()+":"+userInfoVo.getPassWord()).getBytes("UTF-8"));
//            String encoding = DatatypeConverter.printBase64Binary("username:password".getBytes("UTF-8"));  //username  password 自行修改  中间":"不可少
                    httpPost.setHeader("Authorization", "Basic " + encoding);
                    HttpEntity entityParam = new StringEntity(jsonPrarms, ContentType.create("application/json", "UTF-8"));  //这里的“application/json” 可以更换因为本人是传的json参数所以用的这个
                    httpPost.setEntity(entityParam);     //把参数添加到post请求
                    HttpResponse response = httpClient.execute(httpPost);

                    StatusLine statusLine = response.getStatusLine();   //获取请求对象中的响应行对象
                    int responseCode = statusLine.getStatusCode();

                    if(200==responseCode){
                        System.out.println("ok");
                        Map<String,Object> innerMap= new HashMap<>();
                        String docNumber= UUID.randomUUID().toString().replace("-", "");
                        String docName=docNumber+new Date().getTime();
                        docAttrs.put("projectName", pjName);
                        docAttrs.put("docNumber",pjNum);
                        docAttrs.put("docName", docName);

                        HttpUtils.webUpload(fileUrl,docAttrs,HttpUtils.createFileItem(downloadUrl,"test1105"),encoding);
                    }

                }
            }

        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        } catch (Exception e) {
            e.printStackTrace();
        }


    }
}
