package com.snail.fileupload;

import com.alibaba.fastjson.JSONObject;

import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;
import java.util.HashMap;
import java.util.Map;

@Component
public class AccessTokenCommon {

    public final static String ACCESS_TOKEN_URL = "https://api.dingtalk.com/v1.0/oauth2/accessToken";
    public final static String AppKey = "ding5csbyknbbhkf3but";
    public final static String AppSecret = "wdERb7JJMvWF9kqLvL6buQXKUrcT5g4R5Ye1yiesGeI3dbwnNqNEUxuDI2X1JGm9";
    public  static String ACCESS_TOKEN=null;
    @Scheduled(cron = "* * 2 * * ?")//定时器Quartz,表示两个小时获取执行一次这个方法,这个是关键(启动类上还要有个注解@EnableScheduling)
    @PostConstruct
    public void getAccessToken() {
//        System.out.println(1);
        //替换变量appid和APPSECRET
        Map<String,Object> parms= new HashMap<>();
        parms.put("appKey",AppKey);
        parms.put("appSecret",AppSecret);
//        Map<String,String>  heaser= new HashMap<>();
//        heaser.put("Content-type", "application/json");
        JSONObject json = new JSONObject(parms);
        JSONObject jsonObject = JSONObject.parseObject(HttpUtils.postJson(ACCESS_TOKEN_URL,json.toJSONString()));
        //使用微信工具类发送GET请求,这里"GET"要大写 ,获取到JSONObject类型
        //获取accexx_token
        AccessTokenCommon.ACCESS_TOKEN = jsonObject.getString("accessToken");
        System.out.println(AccessTokenCommon.ACCESS_TOKEN );
    }
}
