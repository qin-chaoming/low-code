package com.snail.fileupload.service;

import org.springframework.stereotype.Service;
import com.aliyun.tea.*;
import com.aliyun.teautil.*;
import com.aliyun.teautil.models.*;
import com.aliyun.dingtalkyida_1_0.*;
import com.aliyun.dingtalkyida_1_0.models.*;
import com.aliyun.teaopenapi.*;
import com.aliyun.teaopenapi.models.*;

import java.util.Map;

@Service
public class UrlChangeService {




    public static com.aliyun.dingtalkyida_1_0.Client createClient() throws Exception {
        Config config = new Config();
        config.protocol = "https";
        config.regionId = "central";
        return new com.aliyun.dingtalkyida_1_0.Client(config);
    }


    public static String  changeUrl(Map<String,String> urlPrams) throws Exception {
        String downloadUrl="";
        com.aliyun.dingtalkyida_1_0.Client client = UrlChangeService.createClient();
        GetOpenUrlHeaders getOpenUrlHeaders = new GetOpenUrlHeaders();
        getOpenUrlHeaders.xAcsDingtalkAccessToken = urlPrams.get("accessToken");
        GetOpenUrlRequest getOpenUrlRequest = new GetOpenUrlRequest()
                .setSystemToken(urlPrams.get("key"))
                .setUserId(urlPrams.get("userId"))
                .setLanguage("zh_CN")
                .setFileUrl(urlPrams.get("originalUrl"))
                .setTimeout(60000L);
        try {
            GetOpenUrlResponse getOpenUrlResponse=client.getOpenUrlWithOptions(urlPrams.get("appType"), getOpenUrlRequest, getOpenUrlHeaders, new RuntimeOptions());
            downloadUrl= getOpenUrlResponse.getBody().getResult();
        } catch (TeaException err) {
            if (!com.aliyun.teautil.Common.empty(err.code) && !com.aliyun.teautil.Common.empty(err.message)) {
                // err 中含有 code 和 message 属性，可帮助开发定位问题
            }

        } catch (Exception _err) {
            _err.printStackTrace();
            TeaException err = new TeaException(_err.getMessage(), _err);
            if (!com.aliyun.teautil.Common.empty(err.code) && !com.aliyun.teautil.Common.empty(err.message)) {
                // err 中含有 code 和 message 属性，可帮助开发定位问题
            }

        }
        return downloadUrl;
    }
}
