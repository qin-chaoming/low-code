String content = (String)input.get("content");
String password = (String)input.get("password");
Integer type = Integer.parseInt(String.valueOf(input.get("type")));
/**
*在这里编写您的业务代码, 也可以将业务代码封装到其他类或方法里.
*/
JSONObject result = new JSONObject();
result.put("success",false);
result.put("result","");
result.put("error","");
if (0 == type) {
/**
* 加密
*/
String encryptContent = DESUtil.encrypt(content, password);
System.out.println("加密后的字符串:" + encryptContent);
if (StringUtils.isEmpty(encryptContent)) {
result.put("error", "empty string got!");
return result;
}
result.put("result", encryptContent);
result.put("success", true);
}
else {
/**
* 解密
*/
String encryptContent = DESUtil.decrypt(content, password);
System.out.println("解密后的字符串:" + encryptContent);
if (StringUtils.isEmpty(encryptContent)) {
result.put("error", "empty string got!");
return result;
}
result.put("result", encryptContent);
result.put("success", true);
}
System.out.println("返回:" + JSON.toJSONString(result));
return result;

