/**
宜搭待办列表功能
宜搭高级的考核中题目
使用不同的远程API获取、更改、创建其他表单中的数据

*/

// 当页面渲染完毕后马上调用下面的函数，这个函数是在当前页面 - 设置 - 生命周期 - 页面加载完成时中被关联的。
export function didMount() {
  console.log(`「页面 JS」：当前页面地址 ${location.href}`);
  // console.log(`「页面 JS」：当前页面 id 参数为 ${this.state.urlParams.id}`);
  // 更多 this 相关 API 请参考：https://www.yuque.com/yida/support/ocmxyv#OCEXd
  // document.title = window.loginUser.userName + ' | 宜搭';
  this.getTodoListData();
  this.getFinishListData();
}
//获取进行中待办数据
export function getTodoListData() {
  this.dataSourceMap.getTodoTasks.load().then(res => {
    console.log('a', res)
    //以下根据返回内容重构数据对象，使数据格式符合宜搭组件格式要求
    let toDoData = []
    for (let i = 0; i < res.data.length; i++) {
      let tmpData = res.data[i].formData
      //此处另外添加formInstId属性，用于之后的复选框功能
      tmpData['formInstId'] = res.data[i].formInstId
      //将每一个重构的对象存入到全局变量ToDoData
      toDoData.push(tmpData)
    }
    //表格展示数据源。其中，currentPage和totalCount的取值会影响到表格分页器的展示
    this.setState({
      toDoListData: {
        data: toDoData,
        currentPage: res.currentPage,
        totalCount: res.totalCount,
      }
    })
    // console.log('v', toDoData)
    //原表单真实数据集，用于后续的搜索功能
    state.ToDoData_1 = toDoData;
  })
  
}
//获取已完成待办数据
export function getFinishListData() {
  this.dataSourceMap.getFinishTasks.load().then(res => {
    //console.log(res)
    //以下根据返回内容重构数据对象，使数据格式符合宜搭组件格式要求
    let doneListData = []
    for (let i = 0; i < res.data.length; i++) {
      let tmpData = res.data[i].formData
      //此处另外添加formInstId属性，用于之后的复选框功能
      tmpData['formInstId'] = res.data[i].formInstId
      //将每一个重构的对象存入到全局变量ToDoData
      doneListData.push(tmpData)
    }
    //表格展示数据源。其中，currentPage和totalCount的取值会影响到表格分页器的展示
    this.setState({
      doneListData: {
        data: doneListData,
        currentPage: res.currentPage,
        totalCount: res.totalCount
      }
    })
    //原表单真实数据集，用于后续的搜索功能
    state.DoneListData_1 = doneListData
  })
}

/**
* tablePc onFetchData
* @param params.currentPage 当前页码
* @param params.pageSize 每页显示条数
* @param params.searchKey 搜索关键字
* @param params.orderColumn 排序列
* @param params.orderType 排序方式（desc,asc）
* @param params.from 触发来源（order,search,pagination）
*/
export function onToDoFetchData(params) {

  // 如果是搜索的话翻页重置到 1
  if (params.from === 'search') {
    params.currentPage = 1;

    // 判断搜索框内是否有值，有则根据搜索框内的内容，对照toDoData_1中的数据集进行比对，这里，将【分类】的值作为搜索的类别。
    if (params.searchKey) {
      let tmpToDoData = state.ToDoData_1
      let tmpTodoArr = []
      //遍历tmpTodoData集合，比较指定键对应的值是否与search属性中的值相等，如果相等，则添加到tmpToDoArr数组中，最后，将数组重新赋值给表格展示数据源toDoListData。
      for (let i = 0; i < tmpToDoData.length; i++) {
        if (tmpToDoData[i]['radioField_lfltrmha'] == params.searchKey) {
          tmpTodoArr.push(tmpToDoData[i])
        }
      }
      this.setState({
        toDoListData: {
          data: tmpTodoArr,
          currentPage: params.currentPage,
          totalCount: tmpTodoArr.length
        }
      })
    }
  }
  this.dataSourceMap['getTodoTasks'].load(params);
}

/**
* tablePc onFetchData
* @param params.currentPage 当前页码
* @param params.pageSize 每页显示条数
* @param params.searchKey 搜索关键字
* @param params.orderColumn 排序列
* @param params.orderType 排序方式（desc,asc）
* @param params.from 触发来源（order,search,pagination）
*/
export function onDoneFetchData(params) {
  // 如果是搜索的话翻页重置到 1
  if (params.from === 'search') {
    params.currentPage = 1;

    //判断搜索框内是否有值，有则根据搜索框内的内容，对照toDoData_1中的数据集进行比对，这里，将【分类】的值作为搜索的类别。
    if (params.searchKey) {
      let tmpToDoData = state.DoneListData_1
      let tmpTodoArr = []
      //遍历tmpTodoData集合，比较指定键对应的值是否与search属性中的值相等，如果相等，则添加到tmpToDoArr数组中，最后，将数组重新赋值给表格展示数据源toDoListData。
      for (let i = 0; i < tmpToDoData.length; i++) {
        if (tmpToDoData[i]['radioField_lfltrmha'] == params.searchKey) {
          tmpTodoArr.push(tmpToDoData[i])
        }
      }
      this.setState({
        toDoListData: {
          data: tmpTodoArr,
          currentPage: params.currentPage,
          totalCount: tmpTodoArr.length
        }
      })
    }
  }
}

export function onAddBarltemClick() {
  this.$('dialog_lflupx9w').show()
}

/**
* dialog onOk
*/
export function updateTodoList() {
  //获取对应组件的输入值
  let title = this.$('textField_lflupx9x').getValue(); //（$()内为你自己的组件标识）
  let type = this.$('radioField_lflupx9y').getValue();
  let degree = this.$('rateField_lflupx9z').getValue();
  let time = this.$('dateField_lflupxa0').getValue();
  let note = this.$('textareaField_lflupxa1').getValue();
  //将要新建的内容转换为json对象
  let dataJson = {
    "textField_lfltrmh9": title,
    "radioField_lfltrmha": type,
    "rateField_lfltrmhb": degree,
    "dateField_lfltrmhc": time,
    "textareaField_lfltrmhd": note
  }
  dataJson = JSON.stringify(dataJson);
  // console.log('c', dataJson)
  //构建新建接口所需的json参数对象
  let params = {
    formUuid: "FORM-K5A66M71VMA9HO7PAYB6E6YVQXTW3X9BRTLFL9",// （你自己的）
    appType: "APP_XLU7PQY61KGRC3CPH2T4",//(你自己的)
    formDataJson: dataJson
  }
  this.dataSourceMap.updateList.load(params).then(res => {
    console.log('%', res);
    this.getTodoListData();
  }).catch(err => {
    console.log('#', err);//打印错误，可选
  })
}

export function onActionClick(rowData) {
  this.$('dialog_lflupxa7').show()

  state.bianjidata = rowData;
}

/**
* dialog onOpen
*/
export function onOpen() {
  let data = state.bianjidata;
  console.log(data['textField_lfkedf7z'])
  this.$('textField_lflupxa2').setValue(data['textField_lfltrmh9'] + "");
  // // this.$('textField_lfkq7anl').setValue("1213"); //（$()内为你自己的组件标识）
  this.$('radioField_lflupxa3').setValue(data['radioField_lfltrmha'] + "");
  this.$('rateField_lflupxa4').setValue(parseInt(data['rateField_lfltrmhb']));
  this.$('dateField_lflupxa5').setValue(parseInt(data['dateField_lfltrmhc']));
  this.$('textareaField_lflupxa6').setValue(data['textareaField_lfltrmhd'] + "");
}

/**
* dialog onOk
*/
export function updateTodoList001() {
  let data = state.bianjidata;
  //获取对应组件的输入值
  let title = this.$('textField_lflupxa2').getValue(); //（$()内为你自己的组件标识）
  let type = this.$('radioField_lflupxa3').getValue();
  let degree = this.$('rateField_lflupxa4').getValue();
  let time = this.$('dateField_lflupxa5').getValue();
  let note = this.$('textareaField_lflupxa6').getValue();
  //将要新建的内容转换为json对象
  let dataJson = {
    "textField_lfltrmh9": title,
    "radioField_lfltrmha": type,
    "rateField_lfltrmhb": degree,
    "dateField_lfltrmhc": time,
    "textareaField_lfltrmhd": note
  }
  dataJson = JSON.stringify(dataJson);
  console.log('c', dataJson)
  //构建新建接口所需的json参数对象
  let params = {
    formInstId: data['formInstId'],// （你自己的）
    updateFormDataJson: dataJson
  }
  this.dataSourceMap.updateList_copy.load(params).then(res => {
    console.log('%', res);
    this.getTodoListData();
  }).catch(err => {
    console.log('#', err);//打印错误，可选
  })
}

export function onDelToDoClick(rowData) {
  this.$('dialog_lflupxa8').show()
  state.toDoRowData = rowData
  // console.log('C', state.toDoRowData);//打印错误，可选
}

/**
* dialog onOk
*/
export function delToDoItem() {
  // console.log('C', state.toDoRowData);//打印错误，可选
  let params = {
    formInstId: state.toDoRowData.formInstId
  }
  // if()
  this.dataSourceMap.deleteToDoList.load(params).then(res => {
    this.getTodoListData()
  }).catch(err => {
    console.log(err)
  })
}

export function onDelDoneClick(rowData) {
  this.$('dialog_lflupxab').show()
  state.toDoRowData = rowData
}

/**
* dialog onOk
*/
export function delDoneItem() {
  // console.log('C', state.toDoRowData);//打印错误，可选
  let params = {
    formInstId: state.toDoRowData.formInstId
  }
  // if()
  this.dataSourceMap.deleteDoneList.load(params).then(res => {
    this.getFinishListData();
  }).catch(err => {
    console.log(err)
  })
}

/**
      * 选择（或取消选择）数据之后的回调
      * @param selectedRowKeys Array 选中行的 key
      * @param records Array 选中行的数据
      */
export function onSelectChange(selectedRowKeys, records) {
  this.setState({
    hvDoneData: records[0]
  })
  console.log('S', state.hvDoneData);//打印错误，可选
  this.updateDoneList(state.hvDoneData)
  this.delToDo1Item(records[0])
}
//更新已完成待办,即在已完成待办表单中新建进行中待办的删除项
export function updateDoneList(data) {
  let dataJson = JSON.stringify(data)
  console.log('%', dataJson)
  let params = {
    formUuid: "FORM-J1A66U81KF99GS0CENT5HDO4ORI23EVYTTLFLP",
    appType: "APP_XLU7PQY61KGRC3CPH2T4",
    formDataJson: dataJson
  }
  this.dataSourceMap.updateDoneList001.load(params).then(res => {
    // console.log('%', res)
    this.getFinishListData()
  }).catch(err => {
    console.log(err)
  })
}
//调用删除API,此处就是前述提到的接口复用，只要在js中调用接口，不在数据源面板设置参数值，就可以通过修改参数值，就可以删除已有的任意一条数据，包括已完成待办。
export function delToDo1Item(data) {
  let params = {
    formInstId: data.formInstId
  }
  this.dataSourceMap.deleteToDoList.load(params).then(res => {
    this.getTodoListData()
  }).catch(err => {
    console.log(err)
  })
}

