# 作业引导功能
### 需求描述
客户需要在头天提交项目报备，里边会涉及多个危险动作
第二天员工需要在作业引导中选择项目根据分类点击开始作业
员工上传完成照片之后不允许更改，直接跳转到下个图片上传
完成上传后直接跳转回默认页面，且不允许再次点击开始作业
### 需求实现
![输入图片说明](images/image.png)危险动作录入
基础危险动作信息
![输入图片说明](images/image1.png)项目报备
关联危险动作，可多选
![输入图片说明](images/image3.png)项目台账
由项目报备生成
![输入图片说明](images/image3.png)作业引导默认界面
![输入图片说明](images/image4.png)作业引导点击按钮实现引导功能