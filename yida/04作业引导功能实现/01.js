/**
* 尊敬的用户，你好：页面 JS 面板是高阶用法，一般不建议普通用户使用，如需使用，请确定你具备研发背景，能够自我排查问题。当然，你也可以咨询身边的技术顾问或者联系宜搭平台的技术支持获得服务（可能收费）。
* 我们可以用 JS 面板来开发一些定制度高功能，比如：调用阿里云接口用来做图像识别、上报用户使用数据（如加载完成打点）等等。
* 你可以点击面板上方的 「使用帮助」了解。
*/

// 当页面渲染完毕后马上调用下面的函数，这个函数是在当前页面 - 设置 - 生命周期 - 页面加载完成时中被关联的。
export function didMount() {
  console.log(`「页面 JS」：当前页面地址 ${location.href}`);
  // console.log(`「页面 JS」：当前页面 id 参数为 ${this.state.urlParams.id}`);
  // 更多 this 相关 API 请参考：https://www.yuque.com/yida/support/ocmxyv#OCEXd
  // document.title = window.loginUser.userName + ' | 宜搭';
} 


export function beforeSubmit({ formDataMap }){
  let name = this.$('textField_li8pyykk').getValue()
  // let items = this.$('multiSelectField_li9wsh8s').getValue()
  let items = this.$('associationFormField_li9zh2sj').getValue()
  this.setState({
    json: {
      "name": name,
      "items": items
    }
  })
}

export function afterSubmit({ submitResult }){
  console.log('测试', this.state.json["items"][0]);

  for (var i = 0; i < this.state.json["items"].length; i++) {

    let tableName = this.state.json["items"][i]["title"]

    console.log('tableName', tableName);

    let params = {
      formUuid: "FORM-NO766591BGZAMKRJDT8P4BQAARRB2CRNYP8ILD1",// （你自己的）
      formInstanceId: this.state.json["items"][i]["instanceId"],// （你自己的）
      tableFieldId: "tableField_li8q0mti",// （你自己的）
      currentPage: 1,// （你自己的）
      pageSize: 50// （你自己的）
    }
    let table = ""
    let table1 = ""
    this.dataSourceMap.getTableData.load(params).then((response) => {
      for (var j = 0; j < response["data"].length; j++) {
        table = response["data"][j]["textField_li8q0mtj"]
        table1 = response["data"][j]["textField_lib5une7"]
        let urlParams = {
          formUuid: "FORM-0IA66C713NWA08L38U1EMDXL73YQ2XFE8Q8IL4",// （你自己的）
          appType: "APP_X6GN1LP7RIEVP97NEBPK",//(你自己的)
          formDataJson: JSON.stringify(
            {
              "textField_li9zj8ce": this.state.json["name"],
              "textField_li9wo5sd": tableName,
              "textField_li9wo5se": table,
              "textField_licmf8ct": table1
            }
          )
        }
        this.dataSourceMap.saveFromData.load(urlParams).then((response) => {
          // console.log('b', response);
        })
      }
      console.log('b', table);
    })
  }
}