/**
* 尊敬的用户，你好：页面 JS 面板是高阶用法，一般不建议普通用户使用，如需使用，请确定你具备研发背景，能够自我排查问题。当然，你也可以咨询身边的技术顾问或者联系宜搭平台的技术支持获得服务（可能收费）。
* 我们可以用 JS 面板来开发一些定制度高功能，比如：调用阿里云接口用来做图像识别、上报用户使用数据（如加载完成打点）等等。
* 你可以点击面板上方的 「使用帮助」了解。
*/

// 当页面渲染完毕后马上调用下面的函数，这个函数是在当前页面 - 设置 - 生命周期 - 页面加载完成时中被关联的。
export function didMount() {
  console.log(`「页面 JS」：当前页面地址 ${location.href}`);
  // console.log(`「页面 JS」：当前页面 id 参数为 ${this.state.urlParams.id}`);
  // 更多 this 相关 API 请参考：https://www.yuque.com/yida/support/ocmxyv#OCEXd
  // document.title = window.loginUser.userName + ' | 宜搭';

  this.$('pageSection_lickng4e').setBehavior("HIDDEN")
}

/**
* button onClick风险过程管控
*/
export function onClick() {
  console.log('onClick');
  let projectName = this.$('textField_lickng44').getValue()
  let titleName = "风险过程管控"
  // this.state.sumPhoto
  this.setState({
    formCurrent: 1
  })

  this.setState({
    sumPhoto: []
  })

  this.setState({
    projectName: projectName
  })

  this.setState({
    titleName: titleName
  })

  this.$('pageSection_lickng4d').setBehavior("HIDDEN")

  this.$('pageSection_lickng4e').setBehavior("NORMAL")

  // debugger;
  // this.getTodoListData("我的测试", projectName1);


  this.dataSourceMap.getData.load().then(res => {
    // console.log('a', res)
    //以下根据返回内容重构数据对象，使数据格式符合宜搭组件格式要求
    let toDoData = []
    for (let i = 0; i < res.data.length; i++) {
      //&& res.data[i].formData["textField_licmf8ct"] == value1
      if (res.data[i].formData["radioField_lia0a8yh"] != "是" && res.data[i].formData["textField_li9zj8ce"] == projectName && res.data[i].formData["textField_licmf8ct"] == titleName) {
        // debugger;
        let tmpData = res.data[i].formData
        //此处另外添加formInstId属性，用于之后的复选框功能
        tmpData['formInstId'] = res.data[i].formInstId
        //将每一个重构的对象存入到全局变量ToDoData
        toDoData.push(tmpData)
        // debugger;
      }
    }

    //表格展示数据源。其中，currentPage和totalCount的取值会影响到表格分页器的展示
    this.setState({
      toDoListData: {
        data: toDoData,
        currentPage: res.currentPage,
        totalCount: res.totalCount,
      }
    })

    console.log('a', state.toDoListData["data"])
    console.log("aaa", state.toDoListData["data"].length);
    this.setState({
      i: state.toDoListData["data"].length
    })
    console.log("bbb", state.i);

    this.$('textField_licr7vdl').setValue(state.toDoListData["data"][0]["textField_li9wo5se"])
    this.setState({
      projectName1: state.toDoListData["data"][0]["textField_li9wo5se"]
    })
  })

}

/**
* button onClick安全票据
*/
export function onClickOne() {
  let projectName = this.$('textField_lickng44').getValue()
  let titleName = "安全票据"
  this.setState({
    formCurrent: 1
  })
  this.setState({
    sumPhoto: []
  })
  this.setState({
    projectName: projectName
  })

  this.setState({
    titleName: titleName
  })

  this.$('pageSection_lickng4d').setBehavior("HIDDEN")

  this.$('pageSection_lickng4e').setBehavior("NORMAL")

  // debugger;
  // this.getTodoListData("我的测试", projectName1);


  this.dataSourceMap.getData.load().then(res => {
    // console.log('a', res)
    //以下根据返回内容重构数据对象，使数据格式符合宜搭组件格式要求
    let toDoData = []
    for (let i = 0; i < res.data.length; i++) {
      //&& res.data[i].formData["textField_licmf8ct"] == value1
      if (res.data[i].formData["radioField_lia0a8yh"] != "是" && res.data[i].formData["textField_li9zj8ce"] == projectName && res.data[i].formData["textField_licmf8ct"] == titleName) {
        // debugger;
        let tmpData = res.data[i].formData
        //此处另外添加formInstId属性，用于之后的复选框功能
        tmpData['formInstId'] = res.data[i].formInstId
        //将每一个重构的对象存入到全局变量ToDoData
        toDoData.push(tmpData)
        // debugger;
      }
    }

    //表格展示数据源。其中，currentPage和totalCount的取值会影响到表格分页器的展示
    this.setState({
      toDoListData: {
        data: toDoData,
        currentPage: res.currentPage,
        totalCount: res.totalCount,
      }
    })

    console.log('a', state.toDoListData["data"])
    console.log("aaa", state.toDoListData["data"].length);
    this.setState({
      i: state.toDoListData["data"].length
    })
    console.log("bbb", state.i);

    this.$('textField_licr7vdl').setValue(state.toDoListData["data"][0]["textField_li9wo5se"])
    this.setState({
      projectName1: state.toDoListData["data"][0]["textField_li9wo5se"]
    })
  })
}



/**
* button onClick作业前准备
*/
export function onClickThree() {
  let projectName = this.$('textField_lickng44').getValue()
  let titleName = "作业前准备"
  this.setState({
    formCurrent: 1
  })
  this.setState({
    sumPhoto: []
  })
  this.setState({
    projectName: projectName
  })

  this.setState({
    titleName: titleName
  })

  this.$('pageSection_lickng4d').setBehavior("HIDDEN")

  this.$('pageSection_lickng4e').setBehavior("NORMAL")

  // debugger;
  // this.getTodoListData("我的测试", projectName1);


  this.dataSourceMap.getData.load().then(res => {
    // console.log('a', res)
    //以下根据返回内容重构数据对象，使数据格式符合宜搭组件格式要求
    let toDoData = []
    for (let i = 0; i < res.data.length; i++) {
      //&& res.data[i].formData["textField_licmf8ct"] == value1
      if (res.data[i].formData["radioField_lia0a8yh"] != "是" && res.data[i].formData["textField_li9zj8ce"] == projectName && res.data[i].formData["textField_licmf8ct"] == titleName) {
        // debugger;
        let tmpData = res.data[i].formData
        //此处另外添加formInstId属性，用于之后的复选框功能
        tmpData['formInstId'] = res.data[i].formInstId
        //将每一个重构的对象存入到全局变量ToDoData
        toDoData.push(tmpData)
        // debugger;
      }
    }

    //表格展示数据源。其中，currentPage和totalCount的取值会影响到表格分页器的展示
    this.setState({
      toDoListData: {
        data: toDoData,
        currentPage: res.currentPage,
        totalCount: res.totalCount,
      }
    })

    console.log('a', state.toDoListData["data"])
    console.log("aaa", state.toDoListData["data"].length);
    this.setState({
      i: state.toDoListData["data"].length
    })
    console.log("bbb", state.i);

    this.$('textField_licr7vdl').setValue(state.toDoListData["data"][0]["textField_li9wo5se"])
    this.setState({
      projectName1: state.toDoListData["data"][0]["textField_li9wo5se"]
    })
  })
}

/**
* button onClick结束确认
*/
export function onClickFour() {
  console.log('onClick');
  let projectName = this.$('textField_lickng44').getValue()
  let titleName = "作业结束确认"
  this.setState({
    formCurrent: 1
  })
  this.setState({
    sumPhoto: []
  })
  this.setState({
    projectName: projectName
  })

  this.setState({
    titleName: titleName
  })

  this.$('pageSection_lickng4d').setBehavior("HIDDEN")

  this.$('pageSection_lickng4e').setBehavior("NORMAL")

  // debugger;
  // this.getTodoListData("我的测试", projectName1);


  this.dataSourceMap.getData.load().then(res => {
    // console.log('a', res)
    //以下根据返回内容重构数据对象，使数据格式符合宜搭组件格式要求
    let toDoData = []
    for (let i = 0; i < res.data.length; i++) {
      //&& res.data[i].formData["textField_licmf8ct"] == value1
      if (res.data[i].formData["radioField_lia0a8yh"] != "是" && res.data[i].formData["textField_li9zj8ce"] == projectName && res.data[i].formData["textField_licmf8ct"] == titleName) {
        // debugger;
        let tmpData = res.data[i].formData
        //此处另外添加formInstId属性，用于之后的复选框功能
        tmpData['formInstId'] = res.data[i].formInstId
        //将每一个重构的对象存入到全局变量ToDoData
        toDoData.push(tmpData)
        // debugger;
      }
    }

    //表格展示数据源。其中，currentPage和totalCount的取值会影响到表格分页器的展示
    this.setState({
      toDoListData: {
        data: toDoData,
        currentPage: res.currentPage,
        totalCount: res.totalCount,
      }
    })

    console.log('a', state.toDoListData["data"])
    console.log("aaa", state.toDoListData["data"].length);
    this.setState({
      i: state.toDoListData["data"].length
    })
    console.log("bbb", state.i);

    this.$('textField_licr7vdl').setValue(state.toDoListData["data"][0]["textField_li9wo5se"])
    this.setState({
      projectName1: state.toDoListData["data"][0]["textField_li9wo5se"]
    })
  })
}



/**
* imageField onSuccess
* @param file: {Object} 文件
* @param value: {Array} 值
*/
export function onSuccess(file, value) {
  console.log('onSuccess', file, value);
  let j = 0
  j = this.state.i;
  console.log('onChange', j);
  let photoDataList = this.state.sumPhoto
  let photo = this.$('imageField_lickng4f').getValue()
  // let photoData = photo;
  console.log("图片测试", photo);
  for (let i = 0; i < photo.length; i++) {
    photoDataList.push(photo[i])
  }

  this.setState({
    sumPhoto: photoDataList
  })

  console.log("总图片测试", photoDataList);

  let formCurrent = this.state.formCurrent
  if (formCurrent != state.i) {

    this.setState({
      formCurrent: formCurrent + 1
    })
    this.$('textField_licr7vdl').setValue(state.toDoListData["data"][formCurrent]["textField_li9wo5se"])
    // this.$('text_lih3durg').setValue(state.toDoListData["data"][formCurrent]["textField_li9wo5se"])

    this.$("imageField_lickng4f").reset()

    this.setState({
      projectName1: state.toDoListData["data"][formCurrent]["textField_li9wo5se"]
    })
    console.log("测试", this.state.projectName1);
  } else {
    switch (state.titleName) {
      case "安全票据":
        this.$('imageField_licw1yp9').setValue(this.state.sumPhoto)
        this.setState({
          stateButton1: "HIDDEN"
        })
        break;
      case "作业前准备":
        this.$('imageField_licw1ypa').setValue(this.state.sumPhoto)
        this.setState({
          stateButton2: "HIDDEN"
        })
        break;
      case "风险过程管控":
        this.$('imageField_lickng45').setValue(this.state.sumPhoto)
        this.setState({
          stateButton: "HIDDEN"
        })
        break;
      case "作业结束确认":
        this.$('imageField_licw1ypb').setValue(this.state.sumPhoto)
        this.setState({
          stateButton3: "HIDDEN"
        })
        break;
    }

    this.$("imageField_lickng4f").reset()
    this.$('pageSection_lickng4d').setBehavior("NORMAL")
    this.$('pageSection_lickng4e').setBehavior("HIDDEN")

  }

  console.log("点击测试", formCurrent);

  let searchFieldJson = {
    "textField_li9zj8ce": this.state.projectName,//项目名称
    "textField_licmf8ct": this.state.titleName,//危险动作
    "textField_li9wo5se": this.state.projectName1,//步骤名称
  }
  searchFieldJson = JSON.stringify(searchFieldJson);
  let params = {
    formUuid: "FORM-0IA66C713NWA08L38U1EMDXL73YQ2XFE8Q8IL4",
    searchFieldJson: searchFieldJson
  }
  // this.dataSourceMap.updateYesorNo.load(params).then(res => {
  //   console.log("res", res.data);
  // })
}