
using System;
using System.Collections.Generic;
using System.Text;
using H3;


public class D00182022a595feaa364d4c90f01ee74f5709aa_ListViewController: H3.SmartForm.ListViewController
{
    public D00182022a595feaa364d4c90f01ee74f5709aa_ListViewController(H3.SmartForm.ListViewRequest request): base(request)
    {
    }

    protected override void OnLoad(H3.SmartForm.LoadListViewResponse response)
    {
        base.OnLoad(response);
    }

    protected override void OnSubmit(string actionName, H3.SmartForm.ListViewPostValue postValue, H3.SmartForm.SubmitListViewResponse response)
    {
        if(actionName == "COPYTONEW") 
        {
            string staff = this.Request.UserContext.UserId;
            string requestId = this.Request["ObjectIds"] + string.Empty;
            string[] objectIds = requestId.Replace('[',' ').Replace(']', ' ').Replace("\"",string.Empty).Trim().Split(',');

            //是否选择数据
            if(objectIds == null || objectIds.Length == 0)
            {
                response.Errors.Add("没有选中任何数据");
                response.Message = "没有选中任何数据";
                return;
            }

            //查询当前勾选的符合条件的数据
            H3.Data.Filter.Filter filter = new H3.Data.Filter.Filter();
            H3.Data.Filter.And and = new H3.Data.Filter.And();
            and.Add(new H3.Data.Filter.ItemMatcher(H3.DataModel.BizObjectSchema.PropertyName_ObjectId,
                H3.Data.ComparisonOperatorType.In, objectIds));//系统字段

            and.Add(new H3.Data.Filter.ItemMatcher(H3.DataModel.BizObjectSchema.PropertyName_Status,
                H3.Data.ComparisonOperatorType.Equal, H3.DataModel.BizObjectStatus.Effective));//系统字段

            filter.Matcher = and;
            

            H3.DataModel.BizObject[] objects = H3.DataModel.BizObject.GetList(this.Request.Engine, this.Request.UserContext.UserId,
                this.Request.Schema, H3.DataModel.GetListScopeType.GlobalAll,filter);

            if(objects == null || objects.Length == 0)
            {
                response.Message = "您需要选择未被挑入的数据！";
                return;
            }

            string gonghaiSchemaCode = this.Request.Schema.SchemaCode;// "D000685Sk2sbcpeyvtl49lczlviad1pe7";//当前表单
            string kehuSchemaCode = "D001820Swde4nj3bbe12dke35aoz8k8v5";//新表单
            H3.DataModel.BizObjectSchema gonghaiSchema = this.Request.Engine.BizObjectManager.GetPublishedSchema(gonghaiSchemaCode);//当前表单
            H3.DataModel.BizObjectSchema kehuSchema = this.Request.Engine.BizObjectManager.GetPublishedSchema(kehuSchemaCode);//新表单

            //两表字段名匹配识别
            Dictionary < string, string > mappingPropertys_1 = new Dictionary<string, string>();
            Dictionary < string, string > mappingPropertys_2 = new Dictionary<string, string>();
            foreach(H3.DataModel.PropertySchema p1 in this.Request.Schema.Properties)//gonghai公海
            {
                foreach(H3.DataModel.PropertySchema p2 in kehuSchema.Properties)//接收
                {
                    if(p1.DisplayName.Trim() == p2.DisplayName.Trim() && p1.DataType == p2.DataType && p1.DataType != H3.Data.BizDataType.File)//H3.Data.BizDataType.点后面可查看文件类型,附件，图片不会显示在列表上
                    {
                        if(!H3.DataModel.BizObjectSchema.IsBoReservedPropertiesOnly(p2.Name))
                            mappingPropertys_2.Add(p1.Name, p2.Name);
                    }
                }
            }

            //接收表字段赋值
            H3.DataModel.BulkCommit commit2 = new H3.DataModel.BulkCommit();
            foreach(H3.DataModel.BizObject gh in objects)
            {
                //查找待接收表是否存在本条数据
                string duifangZT = "0";//初始化没有数据
                string ObjectIdCZ = gh["ObjectId"] + string.Empty;
                H3.Data.Filter.Filter filter2 = new H3.Data.Filter.Filter();
                H3.Data.Filter.And and2 = new H3.Data.Filter.And();
                and2.Add(new H3.Data.Filter.ItemMatcher(H3.DataModel.BizObjectSchema.PropertyName_ObjectId, H3.Data.ComparisonOperatorType.In, ObjectIdCZ));//
                //  and2.Add(new H3.Data.Filter.ItemMatcher(H3.DataModel.BizObjectSchema.PropertyName_Status, H3.Data.ComparisonOperatorType.Equal, H3.DataModel.BizObjectStatus.Effective));//
                filter2.Matcher = and2;
                H3.DataModel.BizObject[] objects2 = H3.DataModel.BizObject.GetList(this.Request.Engine, this.Request.UserContext.UserId, kehuSchema
                    , H3.DataModel.GetListScopeType.GlobalAll, filter2);
                if(objects2 != null && objects2.Length > 0)
                {
                    duifangZT = "0";
                }
                //  if(duifangZT == "0") //待接收表没有数据处理时
                //   {
                //主表赋值
                // H3.DataModel.BizObject kh = new H3.DataModel.BizObject(this.Engine, kehuSchema, this.Request.UserContext.UserId);//加载待接收表结构
                H3.DataModel.BizObject kh = new H3.DataModel.BizObject(Engine, kehuSchema, H3.Organization.User.SystemUserId);//加载待接收表结构          
                foreach(string key in mappingPropertys_2.Keys)
                {
                    kh[mappingPropertys_2[key]] = gh[key];
                }
                //kh["OwnerId"] = this.Request.UserContext.UserId;;
                // kh.OwnerDeptId = this.Request.Engine.Organization.GetParent(this.Request.UserContext.UserId);
                //  系统字段，需要单独赋值，否则会自动生成
                //kh.ObjectId = gh.ObjectId;
                kh["ObjectId"] = gh["ObjectId"];
                kh["Name"] = gh["Name"];
                //kh["SeqNo"] = gh["SeqNo"];
                kh["CreatedBy"] = gh["CreatedBy"];
                kh["CreatedTime"] = gh["CreatedTime"];
                kh["ModifiedTime"] = gh["ModifiedTime"];
                kh.OwnerId = gh.OwnerId;
                kh.OwnerDeptId = gh.OwnerDeptId;
                kh.Status = H3.DataModel.BizObjectStatus.Effective;

                //子表数据复制
                {
                    List < H3.DataModel.BizObject > newChildBoList = new List<H3.DataModel.BizObject>();
                    //当前表单
                    H3.DataModel.BizObject objinlod = H3.DataModel.BizObject.Load(H3.Organization.User.SystemUserId, this.Engine, gonghaiSchemaCode, ObjectIdCZ, false);//获取当前表主表数据
                    string childSchemaCode = "D001820Fd3400dab5b5c4105a49931ecd158b5ce";//当前表单子表code
                    H3.DataModel.BizObject[] childBoArray = (H3.DataModel.BizObject[]) objinlod[childSchemaCode];//获取子表内已有数据
                    //新表
                    string childSchemaCode2 = "D001820Fx1z4lgidwvpa9p4bv19os4hn6";//新表子表 
                    string ObjectIdCZ2 = "";
                    if(childBoArray != null && childBoArray.Length > 0)//当前表数据不为空时
                    {
                        //子表字段匹配，明确子表字段共同字段映射
                        string gonghaiSchemaCode2 = childSchemaCode;// "D000685Fxffujtpkpnrztc2ez5rdrcv01";//当前表单子表
                        string kehuSchemaCode2 = childSchemaCode2;// "D000685F06bd816d97434055bd92179a1b64b7a1";//新表单子表
                        H3.DataModel.BizObjectSchema gonghaiSchema2 = Engine.BizObjectManager.GetPublishedSchema(gonghaiSchemaCode2);//当前表单
                        H3.DataModel.BizObjectSchema kehuSchema2 = Engine.BizObjectManager.GetPublishedSchema(kehuSchemaCode2);//新表单
                        Dictionary < string, string > mappingPropertys_3 = new Dictionary<string, string>();
                        Dictionary < string, string > mappingPropertys_4 = new Dictionary<string, string>();
                        foreach(H3.DataModel.PropertySchema p3 in gonghaiSchema2.Properties)//gonghai公海
                        {
                            foreach(H3.DataModel.PropertySchema p4 in kehuSchema2.Properties)//接收
                            {　　　　　　　　　　　　　　　　　　//H3.Data.BizDataType.File，附件、图片不会显示在列表上，需要排除掉，附件\图片需要额为上传
                                　　　　　　　　　　　　　　　　　　if(p3.DisplayName.Trim() == p4.DisplayName.Trim() && p3.DataType == p4.DataType && p3.DataType != H3.Data.BizDataType.File)
                                {
                                    if(!H3.DataModel.BizObjectSchema.IsBoReservedPropertiesOnly(p4.Name))
                                        mappingPropertys_4.Add(p3.Name, p4.Name);
                                }
                            }
                        }
                        //子表循环赋值
                        //H3.DataModel.BizObjectSchema childSchema2 = kh.Schema.GetChildSchema(childSchemaCode2);
                        //H3.DataModel.BizObject childBo1 = new H3.DataModel.BizObject(Engine, childSchema2, H3.Organization.User.SystemUserId);//加载新表结构
                        foreach(H3.DataModel.BizObject detail in childBoArray)
                        {
                            H3.DataModel.BizObjectSchema childSchema2 = kh.Schema.GetChildSchema(childSchemaCode2);
                            H3.DataModel.BizObject childBo1 = new H3.DataModel.BizObject(Engine, childSchema2, H3.Organization.User.SystemUserId);//加载新表结构
                            foreach(string key in mappingPropertys_4.Keys)
                            {
                                childBo1[mappingPropertys_4[key]] = detail[key];//对象[泛型[Name]]

                                // childBo1["F0000003"] = detail["F0000003"];
                            }
                            childBo1["ObjectId"] = detail["ObjectId"];
                            ObjectIdCZ2 = detail["ObjectId"] + string.Empty;

                            newChildBoList.Add(childBo1);

                            //子表附件
                            // this.Request.Engine.BizObjectManager.CopyFiles(gonghaiSchemaCode, childSchemaCode, "F0000009", ObjectIdCZ2, kehuSchemaCode, childSchemaCode2, "F0000009", ObjectIdCZ2, true, true);//子表对应附件
                        }

                    }
                    kh[childSchemaCode2] = newChildBoList.ToArray();
                }

                if(duifangZT == "0") //待接收表没有数据处理时
                {
                    kh.Create(commit2);
                }
                if(duifangZT == "1") //待接收表没有数据处理时
                {
                    kh.Update(commit2);
                }

                this.Request.Engine.BizObjectManager.CopyFiles(gonghaiSchemaCode, "", "F0000015", ObjectIdCZ, kehuSchemaCode, "", "F0000015", ObjectIdCZ, true, true);//主表附件传递  
                //  this.Request.Engine.BizObjectManager.CopyFiles(gonghaiSchemaCode, gonghaiSchemaCode2, "F0000009", "原-子表数据ObjectId", kehuSchemaCode, kehuSchemaCode2, "F0000009", "新-子表数据ObjectId", true, true);                 
                //this.Request.Engine.BizObjectManager.CopyFiles("原-主表编码", "", "原-主表内附件控件编码", "原-主表数据ObjectId", "新-主表编码","", "新-主表内附件控件编码", "新-主表数据ObjectId", true, true);//附件传递方法
                //子表内附件控件 复制到 子表内附件控件 上
                //this.Request.Engine.BizObjectManager.CopyFiles("原-主表编码", "原-子表编码", "原-子表内附件控件编码", "原-子表数据ObjectId", "新-主表编码", "新-子表编码", "新-子表内附件控件编码", "新-子表数据ObjectId", true, true);
                /*
                CopyFiles方法最后两个参数详解：
                第一个true：本次复制附件到目标控件是覆盖还是添加，true为覆盖，false为添加
                第二个true：若本次是覆盖模式，目标控件原先附件是否进行物理删除，true为物理删除，false为只删除记录
                */
                // }
            }
            string errorMsg = null;
            commit2.Commit(this.Request.Engine.BizObjectManager, out errorMsg);
            response.Message = "复制成功！";
        }
        base.OnSubmit(actionName, postValue, response);
    }
}