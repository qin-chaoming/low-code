
using System;
using System.Collections.Generic;
using System.Text;
using H3;

public class D001820a419fffdc97f4d15806f26fdc2b43e11: H3.SmartForm.SmartFormController
{
    static int number = 0;
    public D001820a419fffdc97f4d15806f26fdc2b43e11(H3.SmartForm.SmartFormRequest request): base(request)
    {
    }

    protected override void OnLoad(H3.SmartForm.LoadSmartFormResponse response)
    {
        base.OnLoad(response);
    }

    protected override void OnSubmit(string actionName, H3.SmartForm.SmartFormPostValue postValue, H3.SmartForm.SubmitSmartFormResponse response)
    {
        string num = "0";

        DateTime data = DateTime.Now;
        string getData = data.Year.ToString();
        string sql = "select count(*) as num from i_D001820Fbe4a4b8f13654df6bce269a2a7e5767f  where date_format(F0000004, '%Y')='" + getData + "'";
        System.Data.DataTable dtAccount = this.Request.Engine.Query.QueryTable(sql, null);
        if(dtAccount != null && dtAccount.Rows != null && dtAccount.Rows.Count > 0)
        {
            //获取子表条数
            num = dtAccount.Rows[0]["num"] + string.Empty;
            if(int.Parse(num) == 0)
            {
                num = "0";
                number = 0;
            }
        }

        int serialNumber = int.Parse(num);

        H3.DataModel.BizObject[] details = (H3.DataModel.BizObject[]) this.Request.BizObject["D001820Fbe4a4b8f13654df6bce269a2a7e5767f"]; //获取子表属性并强制转换为对象数组
        if(details != null && details.Length > 0)
        {
            List < H3.DataModel.BizObject > lstObject   = new List<H3.DataModel.BizObject>();
            foreach(H3.DataModel.BizObject detail in details)
            {
                if(detail["F0000002"] == string.Empty || detail["F0000002"] == "")
                {
                    ++serialNumber;
                    ++number;
                    if(serialNumber == number)
                    {
                        string snm = (serialNumber + string.Empty).ToString().PadLeft(8, '0');
                        detail["F0000002"] = getData + snm;//修改子表列的属性
                    } else 
                    {
                        string snm = (number + string.Empty).ToString().PadLeft(8, '0');
                        detail["F0000002"] = getData + snm;//修改子表列的属性
                    }

                }
                lstObject.Add(detail);
            }
            //将List转成对象数组，并重新赋值给主表
            this.Request.BizObject["D001820Fbe4a4b8f13654df6bce269a2a7e5767f"] = lstObject.ToArray();
        }

        base.OnSubmit(actionName, postValue, response);
    }
}