
using System;
using System.Collections.Generic;
using System.Text;
using H3;

public class D28666194010dc91cf54b7b8f5da47d9539ea92: H3.SmartForm.SmartFormController
{
    public D28666194010dc91cf54b7b8f5da47d9539ea92(H3.SmartForm.SmartFormRequest request): base(request)
    {
    }

    protected override void OnLoad(H3.SmartForm.LoadSmartFormResponse response)
    {
        base.OnLoad(response);
    }

    protected override void OnSubmit(string actionName, H3.SmartForm.SmartFormPostValue postValue, H3.SmartForm.SubmitSmartFormResponse response)
    {
        //判断前端传来的actionName
        if(actionName == "getDetailsData")
        {
            try 
            {

                response.ReturnData = new Dictionary<string, object>();
                H3.SmartForm.SmartFormRequest request = this.Request;//取出当前请求对象
                H3.IEngine engine = request.Engine;//取出引擎实例
                string details = request["details"] + string.Empty;

                if(string.IsNullOrWhiteSpace(details))
                {
                    throw new Exception("details参数值异常！");
                }

                List < DetailsDatas > detailsDatas = this.Deserialize<List<DetailsDatas>>(details);

                StringBuilder sb = new StringBuilder();
                foreach(DetailsDatas item in detailsDatas)
                {
                    if(sb.Length > 0) sb.Append(", ");
                    sb.Append('\'').Append(item.F0000215).Append('\'');
                }

                string objectids = sb.ToString();
                string sql = "select * from i_D286661F56ba167a7203422086f3f2052c817666 where ParentObjectId in (" + objectids + ")";
                List <  Dictionary < string, object > > itemDetailsList =  new List<  Dictionary < string, object >>();

                System.Data.DataTable dtAccount = this.Request.Engine.Query.QueryTable(sql, null);
                if(dtAccount != null && dtAccount.Rows != null && dtAccount.Rows.Count > 0)
                {
                    foreach(System.Data.DataRow row in dtAccount.Rows)
                    {

                        Dictionary < string, object > itemDetail = new Dictionary<string, object>();

                        string name = row["F0000048"] + string.Empty;
                        string model = row["F0000004"] + string.Empty;
                        double price = Double.Parse(row["F0000007"] + string.Empty);

                        itemDetail["name"] = name;
                        itemDetail["model"] =model;
                        itemDetail["price"] =price;

                        itemDetailsList.Add(itemDetail);
                    }

                    string itemDetailsJson = this.Serialize(itemDetailsList);
                    response.ReturnData.Add("itemDetailsJson", itemDetailsJson);

                } else
                {
                }

            } catch(Exception ex)
            {
                response.Errors.Add(ex.Message);
            }
        }

        base.OnSubmit(actionName, postValue, response);
    }

    //选择产品子表
    public class DetailsDatas
    {
        // 推荐类型
        public string F0000142;

        // 型号
        public string F0000143;

        // 销售数量
        public string F0000144;

        // 含税单价
        public string F0000169;

        // 含税金额
        public double F0000170;

        // 不含税单价
        public string F0000171;

        // 不含税金额
        public double F0000172;

        // 未回款金额
        public double F0000186;

        // 已回款金额
        public string F0000187;

        // 已开票金额
        public string F0000188;

        // 未开票金额
        public double F0000189;

        // 剩余未发货数量
        public double F0000191;

        // 一类推荐
        public double F0000204;

        // 二类推荐
        public double F0000205;

        // 非标类
        public double F0000206;

        // 产品说明
        public string F0000213;

        // 销售指引表主表object
        public string F0000215;

        // ObjectId
        public string ObjectId;
    }

}
