
using System;
using System.Collections.Generic;
using System.Text;
using H3;
using H3.DataModel;

public class D001820baf57d43e9514240a8d9e1baaea99aa5: H3.SmartForm.SmartFormController
{
    public D001820baf57d43e9514240a8d9e1baaea99aa5(H3.SmartForm.SmartFormRequest request): base(request)
    {
    }

    protected override void OnLoad(H3.SmartForm.LoadSmartFormResponse response)
    {
        base.OnLoad(response);
    }

    protected override void OnSubmit(string actionName, H3.SmartForm.SmartFormPostValue postValue, H3.SmartForm.SubmitSmartFormResponse response)
    {
        BizObject pgsjObj = this.Request.BizObject;
        string mysql = "select * from i_D0018203948378f8f9c4f10aa7ef0e06c97c847";
        System.Data.DataTable dtAccount = this.Request.Engine.Query.QueryTable(mysql, null);
        if(dtAccount != null && dtAccount.Rows != null && dtAccount.Rows.Count > 0)
        {
            List < H3.DataModel.BizObject > lstObject = new List<H3.DataModel.BizObject>();
            //循环数据行
            foreach(System.Data.DataRow row in dtAccount.Rows)
            {
                BizObjectSchema yuanshuju = this.Request.Engine.BizObjectManager.GetPublishedSchema("D001820F9268225320464291a27f7f43cdfae41e");//新子表
                BizObject yuanzibiao = new BizObject(this.Request.Engine, yuanshuju, this.Request.UserContext.UserId);
                yuanzibiao["F0000002"] = row["ObjectId"];
                lstObject.Add(yuanzibiao);
            }
            pgsjObj["D001820F9268225320464291a27f7f43cdfae41e"] = lstObject.ToArray();
            pgsjObj.Update();

        } else
        {
            //查询结果为空
        }

        base.OnSubmit(actionName, postValue, response);
    }
}