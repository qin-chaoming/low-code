/* 控件接口说明：
 * 1. 读取控件: this.***,*号输入控件编码;
 * 2. 读取控件的值： this.***.GetValue();
 * 3. 设置控件的值： this.***.SetValue(???);
 * 4. 绑定控件值变化事件： this.***.BindChange(key,function(){})，key是定义唯一的方法名;
 * 5. 解除控件值变化事件： this.***.UnbindChange(key);
 * 6. CheckboxList、DropDownList、RadioButtonList: $.***.AddItem(value,text),$.***.ClearItems();
 */
/* 公共接口：
 * 1. ajax：$.SmartForm.PostForm(actionName,data,callBack,errorBack,async),
 *          actionName:提交的ActionName;data:提交后台的数据;callback:回调函数;errorBack:错误回调函数;async:是否异步;
 * 2. 打开表单：$.IShowForm(schemaCode, objectId, checkIsChange)，
 *          schemaCode:表单编码;objectId;表单数据Id;checkIsChange:关闭时，是否感知变化;
 * 3. 定位接口：$.ILocation();
 */

// 表单插件代码
$.extend( $.JForm, {
    // 加载事件
    OnLoad: function() {
    },

    // 按钮事件
    OnLoadActions: function( actions ) {
    },

    // 提交校验
    OnValidate: function( actionControl ) {

        var that = this;

        //判断按钮编码（即按钮控件编码）
        if( actionControl.Action == "F0000218" ) {

            var details = that.D286661F0b1113b18b164038b876a0611ee7b7ed.GetValue();
            console.log( 'details', details )
            $.SmartForm.PostForm(
                "getDetailsData",
                {
                    "details": details
                },
                //请求成功后，触发本事件
                function( data ) {
                    if( data.Errors && data.Errors.length ) {//后端通过 response.Errors.Add("异常信息") 或者 异常抛出，在此处接收
                        $.IShowError( "错误", JSON.stringify( data.Errors ) );
                    } else {//后端代码执行无误，在此处接收后端的响应值
                        var result = data.ReturnData;//此值对应后端的 response.ReturnData
                        if( result && result[ "itemDetailsJson" ] ) {
                            console.log( '返回的itemDetailsJson', result[ "itemDetailsJson" ] )
                            let itemList = JSON.parse( result[ "itemDetailsJson" ] );
                            that.D286661F60865edc6df24e388283736eaaf38681.ClearRows();
                            that.D286661F2fb41ed8f0924c1f8a6df5b495a7c0d4.ClearRows();
                            for( let item of itemList ) {

                                that.D286661F60865edc6df24e388283736eaaf38681.AddRow( $.IGuid(), {
                                    "D286661F60865edc6df24e388283736eaaf38681.F0000104": item.name,
                                    "D286661F60865edc6df24e388283736eaaf38681.F0000105": item.model,
                                    "D286661F60865edc6df24e388283736eaaf38681.F0000106": item.price
                                });

                                that.D286661F2fb41ed8f0924c1f8a6df5b495a7c0d4.AddRow( $.IGuid(), {
                                    "D286661F2fb41ed8f0924c1f8a6df5b495a7c0d4.F0000128": item.name,
                                    "D286661F2fb41ed8f0924c1f8a6df5b495a7c0d4.F0000127": item.model,
                                    "D286661F2fb41ed8f0924c1f8a6df5b495a7c0d4.F0000130": item.price
                                });


                            }
                        } else {
                            $.IShowError( "错误", "错误" );
                        }
                    }
                },
                //平台底层报错时，触发本事件
                function( error ) {

                    console.log( '错误', JSON.stringify( error ) )
                    $.IShowError( "错误", JSON.stringify( error ) );
                },
                false
            );
            return false;
        }

        return true;

    },

    // 提交前事件
    BeforeSubmit: function( action, postValue ) {
    },

    // 提交后事件
    AfterSubmit: function( action, responseValue ) {
    }
});