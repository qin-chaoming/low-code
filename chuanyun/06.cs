
using System;
using System.Collections.Generic;
using System.Text;
using H3;
using H3.DataModel;


public class D001820Svfwfa6gxq9ny06x6h205bpse0: H3.SmartForm.SmartFormController
{
    public D001820Svfwfa6gxq9ny06x6h205bpse0(H3.SmartForm.SmartFormRequest request): base(request)
    {
    }

    protected override void OnLoad(H3.SmartForm.LoadSmartFormResponse response)
    {
        base.OnLoad(response);
    }

    protected override void OnSubmit(string actionName, H3.SmartForm.SmartFormPostValue postValue, H3.SmartForm.SubmitSmartFormResponse response)
    {
        // string json = "[{\"F0000001\": \"外部测试1\",\"D001820Ffnnln1gds7ywloh185br1n785\": [{\"F0000002\": \"外部测试1\"},{\"F0000002\": \"外部测试1\"}]},{\"F0000001\": \"外部测试2\",\"D001820Ffnnln1gds7ywloh185br1n785\": [{\"F0000002\": \"外部测试2\"},{\"F0000002\": \"外部测试2\"}]},{\"F0000001\": \"外部测试3\",\"D001820Ffnnln1gds7ywloh185br1n785\": [{\"F0000002\": \"外部测试3\"},{\"F0000002\": \"外部测试3\"}]}]";
        // List < MyObjectTest > objects = this.Deserialize<List<MyObjectTest>>(json);


        // foreach(var obj in objects)
        // {
        //     H3.DataModel.BizObjectSchema   schema = this.Request.Engine.BizObjectManager.GetPublishedSchema("D001820Svfwfa6gxq9ny06x6h205bpse0");
        //     //注：H3.Organization.User.SystemUserId   为系统默认用户Id，在定时器中、自定义接口中由于没有当前登录人，所以用这个代替this.Request.UserContext.UserId
        //     H3.DataModel.BizObject pgsjObj = new H3.DataModel.BizObject(this.Request.Engine, schema, this.Request.UserContext.UserId);
        //     List < H3.DataModel.BizObject > lstObject = new List<H3.DataModel.BizObject>();
        //     pgsjObj["F0000001"] = obj.F0000001;
        //     foreach(var detail in obj.D001820Ffnnln1gds7ywloh185br1n785)
        //     {
        //         BizObjectSchema mxschema = this.Request.Engine.BizObjectManager.GetPublishedSchema("D001820Ffnnln1gds7ywloh185br1n785");//新子表
        //         BizObject zxdfmxobj = new BizObject(this.Request.Engine, mxschema, H3.Organization.User.SystemUserId);
        //         zxdfmxobj["F0000002"] = detail["F0000002"];//D001820Fdo0xjwa4xbka9xi78s9prrhw5.F0000008编码D001820Fdo0xjwa4xbka9xi78s9prrhw5.F0000008
        //         lstObject.Add(zxdfmxobj);
        //     }
        //     pgsjObj["D001820Ffnnln1gds7ywloh185br1n785"] = lstObject.ToArray();
        //     pgsjObj.Status = BizObjectStatus.Effective;
        //     // zxdfobj.Create();
        //     pgsjObj.Create();
        // }
        base.OnSubmit(actionName, postValue, response);
    }
}

public class BatchMyApiController: H3.SmartForm.RestApiController
{
    public BatchMyApiController(H3.SmartForm.RestApiRequest request): base(request) { }
    protected override void OnInvoke(string actionName, H3.SmartForm.RestApiResponse response)
    {
        try
        {
            if(actionName == "BatchCreateOneData")
            {
                string stringValue = this.Request.GetValue<string>("BizObject", "defaultValue");
                List < MyObjectTest > objects = this.Deserialize<List<MyObjectTest>>(stringValue);

                response.ReturnData.Add("result", "success");
                response.ReturnData.Add("message", string.Empty);
                //

                Dictionary < string, string > dic = new Dictionary<string, string>();
                foreach(var obj in objects)
                {
                    H3.DataModel.BizObjectSchema   schema = this.Request.Engine.BizObjectManager.GetPublishedSchema("D001820Svfwfa6gxq9ny06x6h205bpse0");
                    //注：H3.Organization.User.SystemUserId   为系统默认用户Id，在定时器中、自定义接口中由于没有当前登录人，所以用这个代替this.Request.UserContext.UserId
                    H3.DataModel.BizObject pgsjObj = new H3.DataModel.BizObject(this.Request.Engine, schema, H3.Organization.User.SystemUserId);
                    pgsjObj["F0000001"] = obj.F0000001;
                    string name = obj.F0000004;

                    string mysql = "select * from i_D001820ac90315a3bcb4d24b28cc45394adeffd where F0000001='" + name + "'";
                    System.Data.DataTable dtAccount = this.Request.Engine.Query.QueryTable(mysql, null);
                    if(dtAccount != null && dtAccount.Rows != null && dtAccount.Rows.Count > 0)
                    {
                        pgsjObj["F0000003"] = dtAccount.Rows[0]["ObjectId"];
                        pgsjObj["F0000004"] = dtAccount.Rows[0]["F0000001"];
                        pgsjObj["F0000005"] = dtAccount.Rows[0]["F0000002"];
                        pgsjObj["F0000006"] = dtAccount.Rows[0]["F0000003"];
                        pgsjObj["F0000007"] = dtAccount.Rows[0]["F0000004"];
                    }

                    List < H3.DataModel.BizObject > lstObject = new List<H3.DataModel.BizObject>();
                    foreach(var detail in obj.D001820Ffnnln1gds7ywloh185br1n785)
                    {
                        //新表赋值
                        BizObjectSchema mxschema = this.Request.Engine.BizObjectManager.GetPublishedSchema("D001820Ffnnln1gds7ywloh185br1n785");//新子表
                        BizObject zxdfmxobj = new BizObject(this.Request.Engine, mxschema, H3.Organization.User.SystemUserId);
                        string zbname = detail["F0000009"];
                        string sql = "select * from i_D001820ac90315a3bcb4d24b28cc45394adeffd where F0000001='" + zbname + "'";
                        System.Data.DataTable dtAccountzb = this.Request.Engine.Query.QueryTable(sql, null);
                        if(dtAccountzb != null && dtAccountzb.Rows != null && dtAccountzb.Rows.Count > 0)
                        {
                            zxdfmxobj["F0000008"] = dtAccountzb.Rows[0]["ObjectId"];
                            zxdfmxobj["F0000009"] = dtAccountzb.Rows[0]["F0000001"];
                            zxdfmxobj["F0000010"] = dtAccountzb.Rows[0]["F0000002"];
                            zxdfmxobj["F0000011"] = dtAccountzb.Rows[0]["F0000003"];
                            zxdfmxobj["F0000012"] = dtAccountzb.Rows[0]["F0000004"];
                        }

                        // zxdfmxobj["F0000002"] = detail["F0000009"];
                        lstObject.Add(zxdfmxobj);
                    }
                    //创建新表
                    pgsjObj["D001820Ffnnln1gds7ywloh185br1n785"] = lstObject.ToArray();
                    pgsjObj.Status = BizObjectStatus.Effective;
                    // zxdfobj.Create();
                    pgsjObj.Create();
                }

                //从第三方传入参数里拿出传回给第三方
                dic.Add("BizObject", stringValue);
                //回复给第三方请求一个 key为“data”值为对象 的结果
                response.ReturnData.Add("data", dic);
            } else
            {
                response.ReturnData.Add("result", "error");
                response.ReturnData.Add("message", "无法处理actionName为“" + actionName + "”的请求！");
            }
        } catch(Exception ex)
        {
            response.ReturnData.Add("result", "error");
            response.ReturnData.Add("message", ex.Message);
        }
    }
}

public class MyObjectTest
{
    public string F0000001;
    public string F0000004;
    public List<Dictionary<string, string >> D001820Ffnnln1gds7ywloh185br1n785;
}