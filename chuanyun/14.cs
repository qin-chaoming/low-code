 //主表获取人员工时
        BizObject pgsjObj = this.Request.BizObject;
        string name = pgsjObj["F0000002"] + string.Empty;//填报人
        string daily = pgsjObj["F0000001"] + string.Empty;//日报信息
        string date = pgsjObj["F0000003"] + string.Empty;//填报日期
        string sql = "select name from h_user where objectid = '" + name + "'";
        System.Data.DataTable dtAccount = this.Request.Engine.Query.QueryTable(sql, null);
        if(dtAccount != null && dtAccount.Rows != null && dtAccount.Rows.Count > 0)
        {
            name = dtAccount.Rows[0]["name"] + string.Empty;
        }

        BizObject[] details = (BizObject[]) pgsjObj["D001820F0ff004286aeb4312a3d6241a865138fe"];//子表信息
        if(details != null && details.Length > 0)
        {
            foreach(BizObject detail in details)
            {
                string projectName = detail["F0000004"] + string.Empty;//项目名称
                string workContent = detail["F0000005"] + string.Empty;//工作内容
                string schedule = detail["F0000007"] + string.Empty;//完成进度
                string webHook = detail["F0000006"] + string.Empty;//机器人编号
                string accessToken = "";

                //获取webHook中的token的值
                int tokenIndex = webHook.IndexOf("access_token=");
                if(tokenIndex != -1)
                {
                    int valueStartIndex = tokenIndex + "access_token=".Length;
                    accessToken = webHook.Substring(valueStartIndex);
                    // 如果还有其他参数，截取到下一个&符号
                    int nextAmpersandIndex = accessToken.IndexOf('&');
                    if(nextAmpersandIndex != -1)
                    {
                        accessToken = accessToken.Substring(0, nextAmpersandIndex);
                    }

                }

                //拼接字符串
                string result = "## " + projectName + "\n > ### 填报信息：\n > ##### 填报人：" + name + "\n > ##### 填报时间：" + date + "\n > ##### 工作进度：" + schedule + "\n > ##### 工作内容： \n > " + workContent + "\n > ##### 日报信息： \n > " + daily;


                //本示例是在表单后端事件中调用，所以H3.IEngine实例可以用this.Engine获取
                H3.IEngine engine = this.Engine;
                //header 请求参数初始化，此实例会添加到请求的 Headers 中
                Dictionary < string, string > headers = new Dictionary<string, string>();
                //query 请求参数初始化，此处添加的参数会附加在请求Url后（例：?code=654028207203）
                Dictionary < string, string > querys = new Dictionary<string, string>();
                //body 请求数据初始化，此实例会转换为JSON格式发送给接口
                Dictionary < string, object > bodys = new Dictionary<string, object>();
                bodys.Add("token",accessToken);
                bodys.Add("text",result);
                bodys.Add("title",projectName);

                //定义响应数据整体结构体
                H3.BizBus.BizStructureSchema structureSchema = new H3.BizBus.BizStructureSchema();
                structureSchema.Add(new H3.BizBus.ItemSchema("code", "结果状态码", H3.Data.BizDataType.Int, null));
                structureSchema.Add(new H3.BizBus.ItemSchema("msg", "描述", H3.Data.BizDataType.String, null));
                structureSchema.Add(new H3.BizBus.ItemSchema("data", "返回数据", H3.Data.BizDataType.String, null));

                //调用Invoke接口，系统底层访问第三方接口的Invoke方法
                H3.BizBus.InvokeResult res = engine.BizBus.InvokeApi(
                    H3.Organization.User.SystemUserId, //固定值，无需改变
                    H3.BizBus.AccessPointType.ThirdConnection, //固定值，无需改变
                    "SetLog", //连接编码，对应 插件中心 中配置的连接的编码
                    "POST", //请求方式，取值：GET / POST

                    //请求数据类型
                    //注意：如果是传递json数据，这里直接用“application/json”
                    "application/json",
                    headers, querys, bodys, structureSchema);
                if(res != null)
                {
                    int resCode = res.Code; //调用是否成功
                    if(resCode == 0)
                    {
                        //获取返回数据，此实例对应完整的响应JSON
                        H3.BizBus.BizStructure resData = res.Data;

                        //获取响应数据中的 data 属性值
                        string value = resData["data"] + string.Empty;
                        this.Request.BizObject["F0000001"] = value;
                    }
                    else
                    {
                        //获取错误信息
                        string resMessage = res.Message;
                        throw new Exception("接口调用失败：" + resMessage);
                    }
                } else
                {
                    throw new Exception("接口响应数据为空！");
                }
            }
        }