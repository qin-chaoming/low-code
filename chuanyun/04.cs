
using System;
using System.Collections.Generic;
using System.Text;
using H3;

public class D0018201d2e1809d8d841efbc66c4d82c8368a7: H3.SmartForm.SmartFormController
{
    public D0018201d2e1809d8d841efbc66c4d82c8368a7(H3.SmartForm.SmartFormRequest request): base(request)
    {
    }

    protected override void OnLoad(H3.SmartForm.LoadSmartFormResponse response)
    {
        base.OnLoad(response);
    }

    protected override void OnSubmit(string actionName, H3.SmartForm.SmartFormPostValue postValue, H3.SmartForm.SubmitSmartFormResponse response)
    {
        H3.DataModel.BizObject[] details = (H3.DataModel.BizObject[]) this.Request.BizObject["D001820F90530da8dc60477e97093f2a704057fe"]; //获取子表属性并强制转换为对象数组
        if(details != null && details.Length > 0)
        {
            List <string> lstObject   = new List<string>();
            foreach(H3.DataModel.BizObject detail in details)
            {
                string data = detail["F0000001"] + string.Empty;
                lstObject.Add(data);
            }

            string listData= string.Join("','",lstObject.ToArray());

            string mysql = "update i_D0018203948378f8f9c4f10aa7ef0e06c97c847 set F0000002=0 where ObjectId not in ('"+listData+"')";
            this.Request.Engine.Query.QueryTable(mysql, null);

        } else
        {

        }


        base.OnSubmit(actionName, postValue, response);
    }
}