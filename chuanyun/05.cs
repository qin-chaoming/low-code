
using System;
using System.Collections.Generic;
using System.Text;
using H3;
using H3.DataModel;


public class D00182082d2e4731e1e4a828f03a387acd4bc57: H3.SmartForm.SmartFormController
{
    public D00182082d2e4731e1e4a828f03a387acd4bc57(H3.SmartForm.SmartFormRequest request): base(request)
    {
    }

    protected override void OnLoad(H3.SmartForm.LoadSmartFormResponse response)
    {
        base.OnLoad(response);
    }

    protected override void OnSubmit(string actionName, H3.SmartForm.SmartFormPostValue postValue, H3.SmartForm.SubmitSmartFormResponse response)
    {
        base.OnSubmit(actionName, postValue, response);
    }
}

public class MyApiController: H3.SmartForm.RestApiController
{
    public MyApiController(H3.SmartForm.RestApiRequest request): base(request) { }
    protected override void OnInvoke(string actionName, H3.SmartForm.RestApiResponse response)
    {
        try
        {
            if(actionName == "CreateOneData")
            {
                string stringValue = this.Request.GetValue<string>("BizObject", "defaultValue");
                MyObject result = this.Deserialize<MyObject>(stringValue);

                response.ReturnData.Add("result", "success");
                response.ReturnData.Add("message", string.Empty);
                //
                H3.DataModel.BizObjectSchema   schema = this.Request.Engine.BizObjectManager.GetPublishedSchema("D00182082d2e4731e1e4a828f03a387acd4bc57");
                //注：H3.Organization.User.SystemUserId   为系统默认用户Id，在定时器中、自定义接口中由于没有当前登录人，所以用这个代替this.Request.UserContext.UserId
                H3.DataModel.BizObject pgsjObj = new H3.DataModel.BizObject(this.Request.Engine, schema, H3.Organization.User.SystemUserId);
                pgsjObj["F0000001"] = result.F0000001;

                List <H3.DataModel.BizObject> lstObject = new List<H3.DataModel.BizObject>();
                foreach(var detail in result.D001820Fcbe99b7ef9ca4f899995d8b4b7f80489)
                {
                    //新表赋值
                    BizObjectSchema mxschema = this.Request.Engine.BizObjectManager.GetPublishedSchema("D001820Fcbe99b7ef9ca4f899995d8b4b7f80489");//新子表
                    BizObject zxdfmxobj = new BizObject(this.Request.Engine, mxschema, H3.Organization.User.SystemUserId);
                    zxdfmxobj["F0000002"] = detail["F0000002"];//D001820Fdo0xjwa4xbka9xi78s9prrhw5.F0000008编码D001820Fdo0xjwa4xbka9xi78s9prrhw5.F0000008
                    lstObject.Add(zxdfmxobj);
                }
                //创建新表
                pgsjObj["D001820Fcbe99b7ef9ca4f899995d8b4b7f80489"] = lstObject.ToArray();
                pgsjObj.Status = BizObjectStatus.Effective;
                // zxdfobj.Create();
                pgsjObj.Create();

                Dictionary < string, string > dic = new Dictionary<string, string>();
                //从第三方传入参数里拿出传回给第三方
                dic.Add("BizObject", result.F0000001);
                //回复给第三方请求一个 key为“data”值为对象 的结果
                response.ReturnData.Add("data", dic);
            } else
            {
                response.ReturnData.Add("result", "error");
                response.ReturnData.Add("message", "无法处理actionName为“" + actionName + "”的请求！");
            }
        } catch(Exception ex)
        {
            response.ReturnData.Add("result", "error");
            response.ReturnData.Add("message", ex.Message);
        }
    }
}

public class MyObject
{
    public string F0000001;
    public List<Dictionary<string, string >> D001820Fcbe99b7ef9ca4f899995d8b4b7f80489;
}