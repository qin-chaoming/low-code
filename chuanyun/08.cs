
using System;
using System.Collections.Generic;
using System.Text;
using H3;
using H3.DataModel;

public class D0018205dd3d853db964242af182d9b18ae41b4: H3.SmartForm.SmartFormController
{
    public D0018205dd3d853db964242af182d9b18ae41b4(H3.SmartForm.SmartFormRequest request): base(request)
    {
    }

    protected override void OnLoad(H3.SmartForm.LoadSmartFormResponse response)
    {
        base.OnLoad(response);
    }

    protected override void OnSubmit(string actionName, H3.SmartForm.SmartFormPostValue postValue, H3.SmartForm.SubmitSmartFormResponse response)
    {
        // string mysql = "select * from i_D0018205dd3d853db964242af182d9b18ae41b4";
        // System.Data.DataTable dtAccount = this.Request.Engine.Query.QueryTable(mysql, null);
        // if(dtAccount != null && dtAccount.Rows != null && dtAccount.Rows.Count > 0)
        // {
        //     foreach(System.Data.DataRow row in dtAccount.Rows)
        //     {
        //         string name = row["F0000002"] + string.Empty;
        //         string id = row["OwnerDeptId"] + string.Empty;
        //     }

        // }
        // string mysql = "update i_D0018205dd3d853db964242af182d9b18ae41b4 set OwnerDeptId='a5a0907d-ad8f-4e4e-b3ae-423558ecc6bf'";
        // this.Request.Engine.Query.QueryTable(mysql, null);

        BizObject pgsjObj = this.Request.BizObject;
        string code = pgsjObj["F0000004"] + string.Empty;

        H3.DataModel.BizObject[] details = (H3.DataModel.BizObject[]) this.Request.BizObject["D001820Fc9c8a273c7eb4f5bba5e61e14235d9d6"]; //获取子表属性并强制转换为对象数组
        if(details != null && details.Length > 0)
        {
            foreach(H3.DataModel.BizObject detail in details)
            {
                string name = detail["F0000006"] + string.Empty;//关联部署专家
                string nameCode = detail["F0000008"] + string.Empty;//关联部署专家_控件编码

                string department = detail["F0000007"] + string.Empty;//关联所属部门
                string departmentCode = detail["F0000009"] + string.Empty;//关联所属部门_控件编码

                string belong = detail["F0000011"] + string.Empty;//所属部门
                string belongCode = detail["F0000010"] + string.Empty;//所属部门_控件编码
                
                string mysql = "update i_" + code + " set "+departmentCode+"='" + department + "',"+belongCode+"='" + belong + "' where "+nameCode+"='" + name + "'";
                this.Request.Engine.Query.QueryTable(mysql, null);
            }
        }
        base.OnSubmit(actionName, postValue, response);
    }
}