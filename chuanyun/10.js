var that = this

        let itemNo = null;
        let matchNumber = null;
        let beforeDot = null;
        let afterDot = null;

        that.D001820Fb9eda0500954468e9f34b6a18285ef48.ClearRows();
        that.F0000001.BindChange( $.IGuid(), function() {
            //获取 文本框F0000001 的值
            var startDate = that.F0000001.GetValue();

            let indexOfDot = startDate.indexOf( '.' );

            if( indexOfDot !== -1 ) {
                beforeDot = startDate.substring( 0, indexOfDot ); // 获取点之前的数据

                // 判断点之前的和原有是否相同
                afterDot = startDate.substring( indexOfDot + 1 ); // 获取点之后的数据

                console.log( '点之前的数据:', beforeDot ); // 输出 A2428956-10P
                console.log( '点之后的数据:', afterDot ); // 输出 S34
            } else {
                console.log( '未找到点符号' );
            }

            if( itemNo == null ) {
                itemNo = beforeDot;

                if( startDate ) {

                    that.F0000003.SetValue( itemNo );//品号赋值

                    var subObjectId = $.IGuid();  //创建行ID
                    var controlManager = that.D001820Fb9eda0500954468e9f34b6a18285ef48;
                    controlManager.AddRow( subObjectId, {
                        "D001820Fb9eda0500954468e9f34b6a18285ef48.F0000002": startDate,
                        "D001820Fb9eda0500954468e9f34b6a18285ef48.F0000004": afterDot
                    })
                    that.F0000001.SetValue( null );

                } else {
                    //若 文本框F0000001 无值，则将 人员单选框F0000003 置空
                    that.F0000001.SetValue( "" );
                }

            } else {
                if( itemNo != beforeDot ) {
                    $.IShowWarn( "警告", "扫描品号不同，请提交后再次扫描！" );//弹出警告消息
                } else {
                    if( startDate ) {

                        that.F0000003.SetValue( itemNo );//品号赋值
                        var subObjectId = $.IGuid();  //创建行ID
                        var controlManager = that.D001820Fb9eda0500954468e9f34b6a18285ef48;
                        controlManager.AddRow( subObjectId, {
                            "D001820Fb9eda0500954468e9f34b6a18285ef48.F0000002": startDate,
                            "D001820Fb9eda0500954468e9f34b6a18285ef48.F0000004": afterDot
                        })
                        that.F0000001.SetValue( null );

                    } else {
                        //若 文本框F0000001 无值，则将 人员单选框F0000003 置空
                        that.F0000001.SetValue( "" );
                    }
                }
            }



        });