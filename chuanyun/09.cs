
        if(actionName == "SetDing")
        {
            string staff = this.Request.UserContext.UserId;
            string requestId = this.Request["ObjectIds"] + string.Empty;
            string[] objectIds = requestId.Replace('[', ' ').Replace(']', ' ').Replace("\"", string.Empty).Trim().Split(',');

            //是否选择数据
            if(objectIds == null || objectIds.Length == 0)
            {
                response.Errors.Add("没有选中任何数据");
                response.Message = "没有选中任何数据";
                return;
            }

            //查询当前勾选的符合条件的数据
            H3.Data.Filter.Filter filter = new H3.Data.Filter.Filter();
            H3.Data.Filter.And and = new H3.Data.Filter.And();
            and.Add(new H3.Data.Filter.ItemMatcher(H3.DataModel.BizObjectSchema.PropertyName_ObjectId,
                H3.Data.ComparisonOperatorType.In, objectIds));//系统字段

            and.Add(new H3.Data.Filter.ItemMatcher(H3.DataModel.BizObjectSchema.PropertyName_Status,
                H3.Data.ComparisonOperatorType.Equal, H3.DataModel.BizObjectStatus.Effective));//系统字段

            filter.Matcher = and;


            H3.DataModel.BizObject[] objects = H3.DataModel.BizObject.GetList(this.Request.Engine, this.Request.UserContext.UserId,
                this.Request.Schema, H3.DataModel.GetListScopeType.GlobalAll, filter);

            if(objects == null || objects.Length == 0)
            {
                response.Message = "您需要选择未被挑入的数据！";
                return;
            }
            string masterSchemaCode = this.Request.Schema.SchemaCode;
            H3.IEngine engine = this.Engine;
            foreach(string objectId in objectIds) 
            {
                // string objectIs = objectId + string.Empty;
                H3.DataModel.BizObject masterBo = H3.DataModel.BizObject.Load(H3.Organization.User.SystemUserId, engine, masterSchemaCode, objectId, false);
                List < H3.Notification.UserMessage > _messageList=new List<H3.Notification.UserMessage>();
                string title = "欢迎词";//标题
                string welcomeWords = masterBo["F0000001"] + string.Empty;//发送内容
                string send = H3.Organization.User.SystemUserId;//发起人
                string receiverid = masterBo["F0000002"] + string.Empty;//接收人
                H3.Notification.UserMessage _message = new H3.Notification.UserMessage(H3.Notification.UserMessageType.DingTalkPlainText, send, receiverid, "", title, welcomeWords, "");
                _messageList.Add(_message);
                if(_messageList.Count > 0)
                {
                    this.Request.Engine.Notifier.Send(_messageList.ToArray());//执行发送
                }
            }

        }