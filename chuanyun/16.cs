 if((actionName == "Submit" && this.Request.BizObject.Status == H3.DataModel.BizObjectStatus.Draft) ||  (actionName == "Submit" &&this.Request.BizObject.Status == H3.DataModel.BizObjectStatus.Effective))
        {
            this.Request.BizObject.Load();
            H3.DataModel.BizObject[] details = (H3.DataModel.BizObject[]) this.Request.BizObject["D001820Ff4d3d84aa50f4c6e8da8739cab56a37f"];
            if(details != null && details.Length > 0)
            {
                List < H3.DataModel.BizObject > lstObject  = new List<H3.DataModel.BizObject>();
                foreach(H3.DataModel.BizObject detail in details)
                {
                    string status = detail["F0000003"] + string.Empty;//获取是否生成任务明细
                    if(status != "是")
                    {
                        H3.IEngine engine = this.Engine;
                        H3.DataModel.BizObjectSchema schema = engine.BizObjectManager.GetPublishedSchema("D001820ce70a0b88e12436e85eb06f87e03194c");
                        string createdBy = this.Request.UserContext.UserId;
                        H3.DataModel.BizObject newBo = new H3.DataModel.BizObject(engine, schema, H3.Organization.User.SystemUserId);
                        newBo.CreatedBy = createdBy;
                        newBo.OwnerId = createdBy;

                        newBo["F0000001"] = detail["ParentObjectId"];
                        newBo["F0000002"] = detail["ObjectId"];

                        newBo["F0000003"] = detail["F0000002"];


                        newBo.WorkflowInstanceId = System.Guid.NewGuid().ToString();
                        newBo.Create();

                        string workItemID = string.Empty;
                        string errorMsg = string.Empty;
                        H3.Workflow.Template.WorkflowTemplate wfTemp = engine.WorkflowTemplateManager.GetDefaultWorkflow(schema.SchemaCode);
                        //发起流程
                        engine.Interactor.OriginateInstance(newBo.OwnerId, schema.SchemaCode, wfTemp.WorkflowVersion, newBo.ObjectId, newBo.WorkflowInstanceId, H3.Workflow.WorkItem.AccessMethod.Web, true, string.Empty, true, out workItemID, out errorMsg);
                        if(!string.IsNullOrEmpty(errorMsg))
                        {
                            throw new Exception("流程实例创建失败：" + errorMsg);
                        }
                        detail["F0000003"] = "是";
                    }
                    lstObject.Add(detail);
                }
                this.Request.BizObject["D001820Ff4d3d84aa50f4c6e8da8739cab56a37f"] = lstObject.ToArray();
                this.Request.BizObject.Update();
            }
        }